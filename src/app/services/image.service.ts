import {Injectable} from "@angular/core";

@Injectable()

export class ImageService {
    public imageUrls: string[] = [];

    constructor() {
        this.setImages();
    }

    public setImages() {
        this.imageUrls.push('http://lorempixel.com/1000/500/cats/1/');
        this.imageUrls.push('http://lorempixel.com/1000/500/cats/2/');
        this.imageUrls.push('http://lorempixel.com/1000/500/cats/3/');
    }
}