import {NgModule} from '@angular/core';
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";

import {AppComponent} from './app.component';
import {SliderComponent} from "./components/slider/slider.component";
import {LastPageComponent} from "./components/last-page/last-page.component";

 import {AppRoutingModule} from "./app-routing.module";

import {ImageService} from "./services/image.service";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
         AppRoutingModule
    ],
    declarations: [
        AppComponent,
        SliderComponent,
        LastPageComponent
    ],

    providers: [ImageService],

    bootstrap: [AppComponent]
})

export class AppModule {}