import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {SliderComponent} from "./components/slider/slider.component";
import {LastPageComponent} from "./components/last-page/last-page.component";


const routes: Routes = [
    {
        path: '',
        component: SliderComponent
    },
    {
        path: 'last-page',
        component: LastPageComponent
    },
    {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule],
    declarations: []
})
export class AppRoutingModule {}
