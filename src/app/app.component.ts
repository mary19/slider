import { Component } from '@angular/core';
import {ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app',
    template: require('./app.component.html'),
    styles: [require('../styles.css')],
    encapsulation: ViewEncapsulation.None
})

export class AppComponent {
}