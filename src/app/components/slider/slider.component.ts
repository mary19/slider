import {Component, OnInit} from '@angular/core';
import {ImageService} from "../../services/image.service";
import {Router} from "@angular/router";

@Component({
    selector: 'my-slider',
    template: require('./slider.component.html'),
    styles: [require('./slider.component.css')]
})

export class SliderComponent implements OnInit {
    public time: number = 12;

    constructor(private service: ImageService, private router: Router) {}

    ngOnInit(): void {
        this.transfer();
    }

    transfer(): void {
        let timerId = setInterval(() => {
            this.time--;
        }, 1000);
        setTimeout(() => {
            clearInterval(timerId);
            this.router.navigate(['last-page'])
        }, 12000);
    }
}