import { Component } from '@angular/core';

@Component({
    selector: 'last-page',
    template: require('./last-page.component.html'),
    styles: [require('./last-page.component.css')]
})

export class LastPageComponent { }